package com.mifhd.logger;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Logger.v("xml>>: (activity_main.xml)");
        Logger.d("xml>>: activity_main", "tes1", "tes2");
        Logger.i("xml>>: activity_main");
        Logger.w("xml>>: activity_main");
        Logger.e("xml>>: activity_main");
        Logger.wtf("xml>>: activity_main");

        LogUtil.v("xml>>: (activity_main.xml)");
        LogUtil.d("xml>>: activity_main");
        LogUtil.i("xml>>: activity_main");
        LogUtil.w("xml>>: activity_main");
        LogUtil.e("xml>>: activity_main");
        LogUtil.wtf("xml>>: activity_main");
    }
}
